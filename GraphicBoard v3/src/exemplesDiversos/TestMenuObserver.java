package exemplesDiversos;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class TestMenuObserver {
	
	public static Field f = new Field();
	public static Window w = new Window(f);
	
	
	public static void main(String[] args) throws InterruptedException {
		
		
		CajaObserver op1 = new CajaObserver("caja1",50, 50, 200, 100, f);
		CajaObserver op2 = new CajaObserver("caja2",50, 150, 200, 200, f);
		Texto t1 = new Texto("t1", 70, 50, 200, 80, "Shipear", f);
		Texto t2 = new Texto("t1", 70, 150, 200, 180, "Salir", f);
		
		while(true) {
			f.draw();
			Thread.sleep(30);
			//mouseClick();
		}
		
	}

	private static void mouseClick() {
		//System.out.println(f.getMouseOverX()+", "+f.getMouseOverY());
		// TODO Auto-generated method stub
		ArrayList<Sprite> listaSpritesClickados = f.getCurrentMouseSprite();
		for(Sprite s : listaSpritesClickados) {
			switch(s.name) {
			case "caja1":
				System.out.println("LUCAS Y ERIC ES EL MEJOR SHIPEO DE LA HISTORIA");
				break;
			case "caja2":
				w.close();
				break;
			}
			
		}
	}
	

}
