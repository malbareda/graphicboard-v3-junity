package exemplesDiversos;

import java.util.Set;

import Core.Field;
import Core.Window;

public class TestLayer {

	
	public static Field f = new Field();
	public static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		
		
		Albertito al = new Albertito("Albert", 0, 0, 50, 50, 0, "resources/joan.jpg", f);
		al.flippedX=true;
		
		Obstaculo o1 = new Obstaculo("o1", 100,100,150,150,0,f);
		Obstaculo o2 = new Obstaculo("o2", 200,100,250,150,0,f);
		Obstaculo o3 = new Obstaculo("o3", 300,100,350,150,0,f);
		
		Terra t = new Terra("Terra", 0,170,1000,950,0,"resources/rainb.jpg",f);

		//literalmente z-index
		al.orderInLayer=50;
		
		//layer
		o1.layer=1;
		o2.layer=2;
		o3.layer=3;
		//puedes asignar layers a los objetos, y puedes pedirle a otros objetos que ignoren su layer
		//tienes que ignorarlos por las dos bandas
		al.ignoredLayers.add(1);
		al.ignoredLayers.add(3);
		o1.ignoredLayers.add(0);
		o3.ignoredLayers.add(0);
		
		
		
		//drawingBox
		//desacopla la capsa de dibuix de la capsa de colisions
		al.collisionBox=true;
		//la capsa de dibuix es 20 pixels mes petita que la de colisions per baix. Aixo implica que "flotara en l'aire"
		al.drawingBoxExtraDown= -20;
		al.drawingBoxExtraLeft= -40;
		//la capsa de dibuix es 20 pixels mes gran que la de colisions per la dreta. Aixo implica que "es podra menjar" l'obstacle per la dreta
		al.drawingBoxExtraRight= 20;
		al.drawingBoxExtraUp= 10;
		
		
		al.setConstantForce(0, 0.2);
		while(true) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			al.move(keys);
			if(w.getKeysDown().contains('w')) {
				al.jump();
			}
		}
	}

	private static Set<Character> input() {
		// TODO Auto-generated method stub
		return w.getPressedKeys();
	}

}
