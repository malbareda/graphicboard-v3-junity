package exemplesDiversos;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Albertito extends PhysicBody{

	public Albertito(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO s'ha de fer
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	public void move(Set<Character> keys) {
		if(keys.contains('d')) {
			this.setVelocity(2, this.velocity[1]);
			flippedX=true;
		}else if(keys.contains('a')){
			this.setVelocity(-2, this.velocity[1]);
			flippedX=false;
		}else {
			this.setVelocity(0, this.velocity[1]);
		}

		
	}

	public void jump() {
		// TODO Auto-generated method stub
		this.addForce(0, -2);
	}

}
