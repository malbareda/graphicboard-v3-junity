package exemplesDiversos.goldenAxer;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Hacha extends PhysicBody{

	int yInicial;
	public Hacha(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
		this.setVelocity(0, 6);
		yInicial = y1;
		this.trigger=true;
	}

	@Override
	public void update() {
		if(y1>=yInicial+70) {
			this.delete();
		}
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		System.out.println("t"+sprite.name);
		if(sprite instanceof Enemigo) {
			((Enemigo) sprite).herir();
		}
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		System.out.println("c"+sprite.name);
		if(sprite instanceof Enemigo) {
			System.out.println("b");
			((Enemigo) sprite).herir();
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
