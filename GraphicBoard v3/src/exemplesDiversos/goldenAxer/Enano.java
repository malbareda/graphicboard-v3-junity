package exemplesDiversos.goldenAxer;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Observer;
import Core.PhysicBody;
import Core.Sprite;

public class Enano extends PhysicBody implements Observer {

	public int spd=4;
	public boolean cooldown = false;
	public boolean click = false;
	
	
	//Timer representa el temporitzador. Només en cal un
    Timer timer = new Timer();
    //tasca. Cada tasca es individual. si vols 5 events necessitaras 5 tasques.
    
    

    private void restablecerCooldown() {
		cooldown=false;
	}
    
    
	
	public Enano(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
		f.w.subscribe(this);
		f.subscribe(this);
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyChar()=='d') {
			this.setVelocity(spd, this.velocity[1]);
		}else if(e.getKeyChar()=='a') {
			this.flippedX=false;
			this.setVelocity(-spd, this.velocity[1]);
		}
		
		
		if(e.getKeyChar()=='w') {
			this.setVelocity(this.velocity[0], -spd);
		}
		else if(e.getKeyChar()=='s') {
			this.setVelocity(this.velocity[0], spd);
		}
		
		if(e.getKeyChar()==' ') {
			
		}
		if(e.getKeyChar()=='z') {
			Hacha h;
			if(!cooldown) {
				if(flippedX==true)
					h = new Hacha("hacha", (int)x2+20,(int)y1-20,(int)x2+20+50,(int)y1-20+50,0,"institut.jpg",f);
				else {
					h = new Hacha("hacha", (int)x1-20-50,(int)y1-20,(int)x1-20,(int)y1-20+50,0,"institut.jpg",f);
				}
				cooldown=true;
				
				
				TimerTask task1 = new TimerTask() {
			        //la funció run indica tot allò que volguem executar
			        @Override
			        public void run()
			        {
			            //en aquest cas volem executar la funció de generarEnemic
			                restablecerCooldown();
			                System.out.println("he ejecutado la tarea");
			        }	
			    };
			    
			    
				timer.schedule(task1, 1000);//estoy llamando a la funcion diciendo, quiero que al cabo de un segundo se ejecute timertask
				//pero no quiero que se ejecute cada segundo
				
			}
		}
		
	}
	
	@Override
	public void update() {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyChar()=='d') {
			this.setVelocity(0, this.velocity[1]);
		}else if(e.getKeyChar()=='a') {
			this.flippedX=true;
			this.setVelocity(0, this.velocity[1]);
		}
		
		
		if(e.getKeyChar()=='w') {
			this.setVelocity(this.velocity[0], 0);
		}
		else if(e.getKeyChar()=='s') {
			this.setVelocity(this.velocity[0], 0);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		Point p = e.getPoint();
		System.out.println(p.x+" "+p.y+" "+x1+" "+y1);
		if(p.x>x1 && p.x<x2 && p.y>y1 && p.y<y2) {
			System.out.println("he clicado en papyrus");
			click=true;
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		click=false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		Point p = e.getPoint();

		if(click) {
			x1=p.x;
			x2=p.x+150;
			y1=p.y;
			y2=p.y+150;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		if(sprite.name.equals("Enemigo")) {
			this.delete();
		}
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
}
