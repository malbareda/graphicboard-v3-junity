package exemplesDiversos;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import Core.Field;
import Core.Observer;
import Core.Sprite;

public class PersonatgeAmbObserver extends Sprite implements Observer{

	public boolean arrastrando = false;
	public PersonatgeAmbObserver(String name, int x1, int y1, int x2, int y2, double angle,  Field f) {
		super(name, x1, y1, x2, y2, angle, "resources/joan.jpg", f);
		System.out.println("aa");
		//IMPORTANTE, SE SUBSCRIBE AL FIELD Y A LA WINDOW EN EL CONSTURCTOR
		f.w.subscribe(this);
		f.subscribe(this);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		System.out.println("He pitjat");
		char caracter = e.getKeyChar();
		System.out.println("char "+caracter);
		int codi = e.getKeyCode();
		System.out.println("codi "+codi);
		String nomTecla = e.getKeyText(codi);
		System.out.println("nom tecla "+nomTecla);
		
		if(nomTecla.equals("Right")) {
			x1++;x2++;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		System.out.println("He alliberat");
		char caracter = e.getKeyChar();
		System.out.println("char "+caracter);
		int codi = e.getKeyCode();
		System.out.println("codi "+codi);
		String nomTecla = e.getKeyText(codi);
		System.out.println("nom tecla "+nomTecla);
		
	}


	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		Point p = e.getPoint();
		int b = e.getButton();
		if(b==1) {
			//boto esquerra
			System.out.println("BOTO ESQUERRE");
		}else if(b==2) {
			//boto mig
			System.out.println("BOTO RODETA");
		}else if(b==3) {
			//boto dret
			System.out.println("BOTO DRET");
		}
		System.out.println("En la posicio " +p.x+","+p.y);
		
		if(this.collidesWithPoint(p.x, p.y)) {
			System.out.println("He pulsado sobre mi mismo");
			arrastrando=true;
		}
		
	}


	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
//		// TODO Auto-generated method stub
//		float ancho = x2-x1;
//		float alto = y2-y1;
//		if(arrastrando) {
//			this.x1 = e.getPoint().x;
//			this.x2 = x1+ancho;
//			this.y1 = e.getPoint().y;
//			this.y2 = y1+alto;
//			arrastrando=false;
//		}
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		Point p = e.getPoint();
		System.out.println("Estamos arrastrando por "+p.x+" , "+p.y);
		
		float ancho = x2-x1;
		float alto = y2-y1;
		
		float oldx1 = x1;
		float oldx2 = x2;
		float oldy1 = y1;
		float oldy2 = y2;
		
			this.x1 = e.getPoint().x;
			this.x2 = x1+ancho;
			this.y1 = e.getPoint().y;
			this.y2 = y1+alto;
			
			
			
			if(!this.collidesWithField().isEmpty() || Math.abs(this.x1-oldx1)>=50 || Math.abs(this.y1-oldy1)>=50) {
				this.x1=oldx1;
				this.x2=oldx2;
				this.y1=oldy1;
				this.y2=oldy2;
			}
		
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	

}
