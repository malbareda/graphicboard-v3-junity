package exemplesDiversos;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class JoanQueraltQueSalta extends PhysicBody{

	public JoanQueraltQueSalta(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, "joan_quadrada.jpg", f);
		// TODO Auto-generated constructor stub
		this.setConstantForce(0, 0.2);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	public void saltar() {
		// TODO Auto-generated method stub
		this.setVelocity(0, 0);
		this.addForce(0, -2);
	}

	public void rotar() {
		// TODO Auto-generated method stub
		this.angle++;
		//this.angle=this.angle%360;
	}

}
