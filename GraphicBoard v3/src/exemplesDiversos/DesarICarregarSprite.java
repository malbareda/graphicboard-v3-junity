package exemplesDiversos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class DesarICarregarSprite {

	// static ArrayList<Murciano> murcianos = new ArrayList<>();
	static int cont=0;
	static Field f = new Field();
	static Window w = new Window(f);
	static MovableChar c = new MovableChar("Char", 20, 20, 50, 50, 0, "resources/link1.gif",f);
	static Rock b = new Rock("Rock", 150, 150, 250, 250, 0, "resources/rock2.png",f);
	static Rock b2 = new Rock("RotatedRock", 350, 150, 450, 250, 45, "resources/rock2.png",f);
	// static Davilillo d = new Davilillo("Davilillo", 350, 300, 400, 350,
	// "davi.gif");
	public static void main(String[] args) throws InterruptedException {
		int contador = 0;
		boolean flag = false;
		while (!flag) {
			c.update();
			input();
			f.draw();

			Thread.sleep(20);

		}
		System.out.println("estas en el lobby");

	}

	private static void input() {
		// TODO Auto-generated method stub
		// devuelve un set con todas las teclas apretadas
		if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('d') || w.getPressedKeys().contains('w')
				|| w.getPressedKeys().contains('s')) {

			if (w.getPressedKeys().contains('a')) {
				c.moveIzq();

			}
			if (w.getPressedKeys().contains('d')) {
				c.moveDer();

			}
			if (w.getPressedKeys().contains('w')) {
				c.moveArr();

			}
			if (w.getPressedKeys().contains('s')) {
				c.moveAba();

			}
			

		}else if(w.getPressedKeys().contains('j')) {
			try {
				System.out.println("??");
				desarLink();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(w.getPressedKeys().contains('k')) {
			try {
				carregarLink();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}else{
			c.doNotMove();
		}

	}

	private static void carregarLink() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		f.removeSprite(c);
		File file = new File("saveLink.melon");
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		c = (MovableChar) ois.readObject();
		ois.close();
		System.out.println(c.getField());
		c.setField(f);
		System.out.println(c.getField());
		
		System.out.println("link carregat");

	}

	private static void desarLink() throws IOException {
		// TODO Auto-generated method stub
		System.out.println("a");
		File f = new File("saveLink.melon");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(c);
		
		oos.close();
		System.out.println("link guardat");
	}

}
