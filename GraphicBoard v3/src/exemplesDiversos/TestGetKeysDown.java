package exemplesDiversos;

import Core.Field;
import Core.Window;

public class TestGetKeysDown {
	
	static int cont=0;
	static Field f = new Field();
	static Window w = new Window(f);
	static MovableChar c = new MovableChar("Char", 20, 20, 50, 50, 0, "resources/link1.gif",f);
	static Rock b = new Rock("Rock", 150, 150, 250, 250, 0, "resources/rock2.png",f);
	static Rock b2 = new Rock("RotatedRock", 350, 150, 450, 250, 45, "resources/rock2.png",f);
	// static Davilillo d = new Davilillo("Davilillo", 350, 300, 400, 350,
	// "davi.gif");
	public static void main(String[] args) throws InterruptedException {
		int contador = 0;
		boolean flag = false;
		while (!flag) {
			c.update();
			input();
			f.draw();

			Thread.sleep(20);

		}
		System.out.println("estas en el lobby");

	}

	private static void input() {
		// TODO Auto-generated method stub
		// devuelve un set con todas las teclas apretadas
		if(w.getKeysDown().contains('w')) {
			c.setVelocity(0, -3);
		}else if(w.getKeysDown().contains('s')) {
			c.setVelocity(0, 3);
		}else if(w.getKeysDown().contains('a')) {
			c.setVelocity(-3, 0);
		}else if(w.getKeysDown().contains('d')) {
			c.setVelocity(3, 0);
		}

	}

}
