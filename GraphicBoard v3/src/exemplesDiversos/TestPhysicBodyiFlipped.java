package exemplesDiversos;

import java.util.Set;

import Core.Field;
import Core.Window;

public class TestPhysicBodyiFlipped {

	
	public static Field f = new Field();
	public static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		
		
		Albertito al = new Albertito("Albert", 0, 0, 50, 50, 0, "resources/joan.jpg", f);
		al.flippedX=true;
		
		Terra t = new Terra("Terra", 0,170,1000,950,0,"resources/rainb.jpg",f);

		
		al.setConstantForce(0, 0.2);
		while(true) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			al.move(keys);
			if(w.getKeysDown().contains('w')) {
				al.jump();
			}
		}
	}

	private static Set<Character> input() {
		// TODO Auto-generated method stub
		return w.getPressedKeys();
	}

}
