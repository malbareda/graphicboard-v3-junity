package exemplesDiversos;

import java.util.Set;

import Core.Field;
import Core.Window;

public class TestSetVelocity {

	public static Field f = new Field();
	public static Window w = new Window(f);

	public static void main(String[] args) throws InterruptedException {

		Albertito al = new Albertito("Albert", 0, 0, 50, 50, 0, "resources/joan.jpg", f);
		Albertito al2 = new Albertito("Albert", 0, 100, 50, 150, 0, "resources/joan.jpg", f);
		Albertito al3 = new Albertito("Albert", 0, 200, 50, 250, 0, "resources/joan.jpg", f);
		Albertito al4 = new Albertito("Albert", 0, 300, 50, 350, 0, "resources/joan.jpg", f);
		
		al.setVelocity(2, 0);
		al2.setVelocity(1.5, 0);
		al3.setVelocity(1.0, 0);
		al4.setVelocity(0.5, 0);
		
		while (true) {
			f.draw();
			Thread.sleep(30);
		}
	}

	

	private static Set<Character> input() {
		// TODO Auto-generated method stub
		return w.getPressedKeys();
	}

}
