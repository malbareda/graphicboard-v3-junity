package exemplesDiversos;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import Core.Field;
import Core.Observer;
import Core.Sprite;

public class CajaObserver extends Sprite implements Observer{

	public boolean toggled = false;
	public CajaObserver(String n, int x1, int y1, int x2, int y2, Field f) {
		super(n, x1, y1, x2, y2,0, "resources/textbox.png", f);
		// TODO Auto-generated constructor stub
		this.f.subscribe(this);
		this.f.w.subscribe(this);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		Point p = e.getPoint();
		if(this.collidesWithPoint(p.x, p.y)) {
			
			toggled = !toggled;
			System.out.println(toggled);
			if(toggled) {
				this.path="resources/textbox.png";
			}else {
				this.path="resources/swap.png";
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
