package exemplesDiversos;

import java.awt.Font;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class TestMouse {
	
	static Field f = new Field();
	static Window w = new Window(f);

	static Texto t = new Texto("texto", 50, 50, 75, 75, "sadadasdadasd",f);
	static Texto t2 = new Texto("texto", 75, 75, 100, 100, "sadadasdadasd",f);
	static Texto t3 = new Texto("texto", 100, 100, 125, 125, "sadadasdadasd",f);
	static Texto t4 = new Texto("texto", 125, 125, 150, 150, "sadadasdadasd",f);

	static Rock b = new Rock("Rock", 150, 150, 250, 250, 0, "resources/rock2.png",f);
	static Rock b2 = new Rock("RotatedRock", 350, 150, 450, 250, 45, "resources/rock2.png",f);
	
	public static void main(String[] args) throws InterruptedException {
		Font fuente = new Font("Monospaced", Font.BOLD, 18);
		int color = 0x0000FF;
		t.font=fuente;
		t.textColor=color;
		w.changeSize(1000, 300);
		
		
		while(true){

			ArrayList<Sprite> sprites = new ArrayList<>();
			
			//ultima posició clickada amb click esquerre
			int x = f.getMouseX();
			int y = f.getMouseY();
			//ultima posició clickada amb click esquerre
			int rx = f.getRightMouseX();
			int ry = f.getRightMouseY();
			//posicio actual del ratoli
			int ox = f.getMouseOverX();
			int oy = f.getMouseOverY();
			
			String str = "l'ultim pixel premut amb el boto esquerre es "+x+", "+y+"\n, i amb el dret "+rx+", "+ry;
			t.path=str;
			sprites.add(t);
			sprites.add(b);
			sprites.add(b2);
			
			String str2="He clickat a sobre dels Sprites: ";

			for(Sprite s: f.getMouseSprite()){
					str2+=s.name+" ";
			}
			t2.path=str2;
			sprites.add(t2);
			
			
			
			String str3 = "El ratoli esta fent mouseOver sobre el pixel"+ox+", "+oy+"\n";
			t3.path=str3;
			sprites.add(t3);
			
			String str4="El ratoli esta sobre dels sprites a sobre dels Sprites: ";
			for(Sprite s: f.getMouseOverSprite()){
				str2+=s.name+" ";
			}
			t4.path=str4;
			sprites.add(t4);
			
			f.draw(sprites);
			
			
			
			
			Thread.sleep(50);
			
		}
	}

}
