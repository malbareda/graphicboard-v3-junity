package exemplesDiversos;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Window;

public class TestTimer {
	
	static Field f = new Field();
	static Window w = new Window(f);
	static Timer timer = new Timer();
	static JoanQueraltQueSalta jq = new JoanQueraltQueSalta("Joan", 100, 400, 200, 500, 0, "", f);

    //tasca. Cada tasca es individual. si vols 5 events necessitaras 5 tasques.
    static TimerTask task1 = new TimerTask() {
        //la funció run indica tot allò que volguem executar
        @Override
        public void run()
        {
            //en aquest cas volem executar la funció de generarEnemic
                jq.saltar();
        }
    };
    
    static TimerTask task2 = new TimerTask() {
        //la funció run indica tot allò que volguem executar
        @Override
        public void run()
        {
            //en aquest cas volem executar la funció de generarEnemic
                jq.rotar();
        }
    };

    
	public static void main(String[] args) throws InterruptedException {
		
		//el primer salto es inmediato, y luego cada dos segundos
		timer.schedule(task1, 0, 1300);
		timer.schedule(task2, 0, 200);

		while(true) {
			Thread.sleep(30);
			f.draw();
		}
		
	}

}
