package exemplesDiversos;


import Core.Field;
import Core.Window;

public class TestObserver {
	
	static Field f = new Field();
	static Window w = new Window(f);
	
	static PersonatgeAmbObserver po = new PersonatgeAmbObserver("po", 100, 100, 150, 150, 0, f); 
	
	static Rock a = new Rock("a", 400, 0, 500, 600, 0, "resources/swap.png", f);
	
	public static void main(String[] args) throws InterruptedException {

		while(true) {
			f.draw();
			Thread.sleep(30);
		}
	}

}
