package exemplesDiversos;

import java.io.Serializable;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class MovableChar extends PhysicBody implements Serializable{

	int cont = 0;
	char direccion='d';
	int cooldown=25; 
	int estat = 0;  //0 suelo, 1 subiendo  2 cayendo
	public MovableChar(String name, int x1, int y1, int x2, int y2, double radi,String path, Field f) {
		super(name, x1, y1, x2, y2, radi, path, f);
		// TODO Auto-generated constructor stub
		//this.trigger=true;
		this.orderInLayer=50;
	}

	public void moveIzq() {
		this.setVelocity(-2, 0);
		direccion='a';
	}

	public void moveDer() {
		// TODO Auto-generated method stub
		this.setVelocity(2, 0);
		direccion='d';
	}
	public void moveArr() {
		this.setVelocity(0, -2);
		direccion='w';
		
	}public void moveAba() {
		this.setVelocity(0, 2);
		direccion='s';
		
	}
	public void doNotMove() {
		this.setVelocity(0, 0);
		direccion='s';
		
	}



	
	public void update() {
		if(cooldown<25)cooldown++;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		cont++;
		System.out.println("He colisionado con "+sprite.name+" numero de colision: "+cont);
		System.out.println("porcentaje de colision "+this.collidesWithPercent(sprite));

	}
	@Override
	public void onCollisionStay(Sprite sprite) {
		
		System.out.println("ESTOY colisionando con "+sprite.name+" numero de colision: "+cont);
		System.out.println("porcentaje de colision "+this.collidesWithPercent(sprite));
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		System.out.println("He dejado de colisionar con"+sprite.name+" numero de colision: "+cont);
		System.out.println("porcentaje de colision "+this.collidesWithPercent(sprite));
	
	}
	

}
